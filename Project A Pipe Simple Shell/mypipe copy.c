#include<stdio.h>
#include<string.h>
#include<sys/wait.h>
#include<unistd.h>
#include <errno.h>
#include<stdlib.h>
#include <sys/types.h>
#define BUFFER_SIZE 80
#define ARR_SIZE 80

void parse_args(char *buffer, char** args, 
                size_t args_size, size_t *nargs)
{
    char *buf_args[args_size]; 
    
    char *wbuf = buffer;
    buf_args[0]=buffer; 
    args[0]=buffer;  /* First argument */
/*
 *  The following replaces delimiting characters with '\0'. 
 *  Example:  " Aloha World\n" becomes "\0Aloha\0World\0\0"
 *  Note that the for-loop stops when it finds a '\0' or it
 *  reaches the end of the buffer.
 */   
    for(char **cp=buf_args; (*cp=strsep(&wbuf, " \n\t")) != NULL ;){
        if ((**cp != '\0') && (++cp >= &buf_args[args_size]))
            break; 
    }

/* Copy 'buf_args' into 'args' */    
    size_t j=0;
    for (size_t i=0; buf_args[i]!=NULL; i++){ 
        if(strlen(buf_args[i])>0)  /* Store only non-empty tokens */
            args[j++]=buf_args[i];
    }
    
    *nargs=j;
    args[j]=NULL;
}

int main(int argc, char* argv[], char* envp[]){
    /*sshell vars*/
    char buffer[BUFFER_SIZE];
    char *args[ARR_SIZE];
    size_t num_args;
    pid_t pid;
    
    /*Pipe vars*/
    int fd[2];

    if(pipe(fd)==-1){
        return 1;
    }

    int pid1=fork();
    if(pid1 < 0){
        return 2;
    }
    if(pid1 == 0){
        //child process 1
        dup2(fd[1], STDOUT_FILENO);
        close(fd[0]);
        close(fd[1]);
        //command 1
        //replace this execlp arguments with args before "|"
        execlp("ping","ping","-c","5","google.com",NULL);
    }
    int pid2=fork();
    if(pid2<0){
        return 3;
    }
    if(pid2 == 0){
        //child process 2
        dup2(fd[0],STDIN_FILENO);
        close(fd[0]);
        close(fd[1]);
        //command two
        //replace this execlp arguments with args after "|"
        execlp("grep","grep","rtt",NULL);
    }

    close(fd[0]);
    close(fd[1]);

    waitpid(pid1,NULL,0);
    waitpid(pid2,NULL,0);
    return 0;
}