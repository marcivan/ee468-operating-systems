/*
 *  This is a simple shell program from
 *  rik0.altervista.org/snippetss/csimpleshell.html
 *  It's been modified a bit and comments were added.
 *
 *  But it doesn't allow misdirection, e.g., <, >, >>, or |
 *  The project is to fix this.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <errno.h>
#include <sys/types.h>
#define BUFFER_SIZE 80
#define ARR_SIZE 80
//#define DEBUG
// #define DEBUG 1  /* In case you want debug messages */

void parse_args(char *buffer, char** args, 
                size_t args_size, size_t *nargs)
{
    char *buf_args[args_size]; 
    
    char *wbuf = buffer;
    buf_args[0]=buffer; 
    args[0]=buffer;  /* First argument */
/*
 *  The following replaces delimiting characters with '\0'. 
 *  Example:  " Aloha World\n" becomes "\0Aloha\0World\0\0"
 *  Note that the for-loop stops when it finds a '\0' or it
 *  reaches the end of the buffer.
 */   
    for(char **cp=buf_args; (*cp=strsep(&wbuf, " \n\t")) != NULL ;){
        if ((**cp != '\0') && (++cp >= &buf_args[args_size]))
            break; 
    }

/* Copy 'buf_args' into 'args' */    
    size_t j=0;
    for (size_t i=0; buf_args[i]!=NULL; i++){ 
        if(strlen(buf_args[i])>0)  /* Store only non-empty tokens */
            args[j++]=buf_args[i];
    }
    
    *nargs=j;
    args[j]=NULL;
}

int main(int argc, char *argv[], char *envp[]){
    char buffer[BUFFER_SIZE];
    char *args[ARR_SIZE];
    char*args1[ARR_SIZE];//tokens before a pipe
    char*args2[ARR_SIZE];//tokens AFTER a pipe
    size_t num_args;
    pid_t pid;
    
    while(1){
        printf("ee468>> "); 
        fgets(buffer, BUFFER_SIZE, stdin); /* Read in command line */
        parse_args(buffer, args, ARR_SIZE, &num_args);//puts words in arg 
            //separate args to args1 and args2
            for(int i = 0; i < ARR_SIZE && args[i]!= NULL; i++){
                if(strcmp(args[i],"|") != 0){
                    args1[i] = args[i];
                }
                if(strcmp(args[i],"|") == 0 ){//If we detect a pipe
                    for(int j = i + 1; j < ARR_SIZE; j++){//create args2
                        args2[j] = args[j];
                    }
                    break;//dont collect more for args1;
                }
            }
        #ifdef DEBUG /*Checks args1 & args2*/
            printf("args1:\n");
            for(int i = 0; i < ARR_SIZE; i++){
                printf("%s\n",args1[i]);
            }
            printf("args2:\n");
            for(int i = 0; i < ARR_SIZE; i++){
                printf("%s\n",args2[i]);
            }
        #endif
        if(!strcmp(args[0], "exit" )) exit(0);
        /*Create pipes*/
        int fd[2];

        if(pipe(fd)==-1){
            return 1;
        }

        int pid1=fork();
        if(pid1 < 0){
            return 2;
        }
        if(pid1 == 0){
            //child process 1
            dup2(fd[1], STDOUT_FILENO);
            close(fd[0]);
            close(fd[1]);
            //command 1
            //replace this execlp arguments with args before "|"
            execvp(args1[0],args1);
        }
        int pid2=fork();
        if(pid2<0){
            return 3;
        }
        if(pid2 == 0){
            //child process 2
            dup2(fd[0],STDIN_FILENO);
            close(fd[0]);
            close(fd[1]);
            //command two
            //replace this execlp arguments with args after "|"
            execvp(args2[0],args);
        }
        close(fd[0]);
        close(fd[1]);
        waitpid(pid1,NULL,0);
        waitpid(pid2,NULL,0); 
    }      
    return 0; 
}