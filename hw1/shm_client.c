/*
	This client program:
	1. Takes a word input 
	2. Sends to shm_server
	3. send 'd' to server to display
	4. send 't' to server to terminate
*/

#include <sys/types.h> /* for shared memory */
#include <sys/ipc.h>   /* for shared memory */
#include <sys/shm.h>   /* for shared memory */
#include <stdio.h>
#include <stdlib.h>  /* for exit() */
#include <unistd.h>  /* for sleep() */
#include <string.h>
#include <signal.h>
#include<sys/socket.h>
#include<arpa/inet.h> /*definitions for internet operations*/

#define BUFFER_SIZE 80
#define ARR_SIZE 80
#define DEBUG
#define MAXDATASIZE 100
#define PORT 8080

int main(int argc, char const* argv[])
{
    int sock = 0, valread, client_fd;
    struct sockaddr_in serv_addr;
    char* clientMessage = "Hello from client";
    char buffer[1024] = { 0 };
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
  
    // Convert IPv4 and IPv6 addresses from text to binary
    // form
    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)
        <= 0) {
        printf(
            "\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if ((client_fd
         = connect(sock, (struct sockaddr*)&serv_addr,
                   sizeof(serv_addr)))
        < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }
	printf("Send d to display, send t to terminate >>");
	//fgets(buffer,BUFFER_SIZE,stdin);
	scanf("%c",&clientMessage);
    send(sock, clientMessage, strlen(clientMessage), 0);
    printf("Client message sent\n");
    valread = read(sock, buffer, 1024);
    printf("%s\n", buffer);
  
    // closing the connected socket
    close(client_fd);
    return 0;
}

