/*  
 *  This server program will create a shared memory block shm[]
 *  that can be accessed by client programs.  
 *  Clients can send commands in shm[0]:
 *     shm[0] = 't' means terminate the server
 *     shm[1] = 'd' means display the character string in shm[]
 *                  starting from shm[1].  The maximum string length
 *                  is 49 bytes or terminated by '0\'.
 *  When the server completes a command, it sets shm[0] to 'c'
 */

#include <sys/types.h> /* for shared memory */
#include <sys/ipc.h>   /* for shared memory */
#include <sys/shm.h>   /* for shared memory */
#include <stdio.h>
#include <stdlib.h>  /* for exit() */
#include <unistd.h>  /* for sleep() */
#include<string.h>
#define SHMSZ 50 /* Shared memory size */
#define SHMNAME 5678 /* Name for the shared memory */

/*For Sockets*/
#include <sys/socket.h>
#include <netinet/in.h>
#define PORT 8080 //tcp port number
#define MAXDATASIZE 100 //bytes

void main(int argc, char const* argv[]) 
{
    /*socket vars*/
	int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffy[1024] = { 0 };
    char* hello = "Hello from server";
    /*Server Stuff*/
   // Creating socket file descriptor
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0))
        == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
  
    // Forcefully attaching socket to the port 8080
    if (setsockopt(server_fd, SOL_SOCKET,
                   SO_REUSEADDR | SO_REUSEPORT, &opt,
                   sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);
  
    // Forcefully attaching socket to the port 8080
    if (bind(server_fd, (struct sockaddr*)&address,
             sizeof(address))
        < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    if ((new_socket
         = accept(server_fd, (struct sockaddr*)&address,
                  (socklen_t*)&addrlen))
        < 0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    valread = read(new_socket, buffy, 1024);
    printf("%s\n", buffy);
    send(new_socket, hello, strlen(hello), 0);
    printf("Hello message sent\n");
    /*End Server Stuff*/
    
    int id;  /* shared memory id */
    key_t key;
    char *shm;
    int k;
    char buffer[SHMSZ];  /* Local buffer for displaying char string */

    /* Create shared memory */
    id = shmget(SHMNAME, SHMSZ, IPC_CREAT | 0666);
    if ( id < 0) {
    perror("shmget");
    printf("Server: Creation of shared memory failed\n");
    exit(1);
    }

    /* Attached shared memory */
    shm = shmat(id, NULL, 0);
    if (shm == (char *)-1) { 
    perror("shmat");
    printf("Server: Couldn't attach shared memory\n");
    exit(1);
    }

    printf("Server: shared memory %d is on\n",SHMNAME);
    for (shm[0]='c'; shm[0] != 't'; sleep(1)) {
        if (shm[0] == 'd') { /* Display.  First copy to buffer */
            printf("Server: display\n");	
            for (k=0; (k<SHMSZ-1) && (shm[k+1]!='\0'); k++) {
                buffer[k] = shm[k+1];
            }
            shm[k+1] = '\0';
            printf("Server: shm = %s\n", buffer);
            shm[0] = 'c';
        }
    }

    shmdt(shm); /* detach shared memory */
    shmctl(id, IPC_RMID, NULL); /* remove shared memory */

    if (shm[0]=='t'){ 
        // closing the connected socket
        close(new_socket);
        // closing the listening socket
        shutdown(server_fd, SHUT_RDWR);
    }
    printf("Server: exit\n");
}


